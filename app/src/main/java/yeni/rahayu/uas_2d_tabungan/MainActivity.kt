package yeni.rahayu.uas_2d_tabungan
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnAnggota.setOnClickListener(this)
        //  btnProdi.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btnAnggota -> {
                var intent = Intent(this, AnggotaActivity::class.java)
                startActivity(intent)
            }
        }}}
package yeni.rahayu.uas_2d_tabungan

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_anggota.*

class AdapterDataAnggota(val dataMhs: List<HashMap<String,String>>,
                         val mahasiswaActivity: AnggotaActivity) : //new
    RecyclerView.Adapter<AdapterDataAnggota.HolderDataMhs>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataMhs {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_anggota, p0, false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(p0: AdapterDataAnggota.HolderDataMhs, p1: Int) {
        val data = dataMhs.get(p1)
        p0.txNim.setText(data.get("id_anggota"))
        p0.txNama.setText(data.get("nama"))
        p0.txTrx.setText(data.get("nama_trx"))
        p0.txAlamat.setText(data.get("alamat"))
        p0.txTelp.setText(data.get("telepon"))
        p0.txJK.setText(data.get("kelamin"))

        //beginNew
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230, 245, 240)
        )
        else p0.cLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            val pos = mahasiswaActivity.daftarTrx.indexOf(data.get("nama_trx"))
            mahasiswaActivity.spinTrx.setSelection(pos)
            val pos1 = mahasiswaActivity.daftarJK.indexOf(data.get("kelamin"))
            mahasiswaActivity.spinJK.setSelection(pos1)
            mahasiswaActivity.edIdAnggota.setText(data.get("id_anggota"))
            mahasiswaActivity.edNama.setText(data.get("nama"))
            mahasiswaActivity.edAlamat.setText(data.get("alamat"))
            mahasiswaActivity.edTelp.setText(data.get("telepon"))
            Picasso.get().load(data.get("url")).into(mahasiswaActivity.imUP)
        })
        //endNew
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataMhs(v: View) : RecyclerView.ViewHolder(v) {
        val txNim = v.findViewById<TextView>(R.id.txIdAnggota)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txTrx = v.findViewById<TextView>(R.id.txTrx)
        val photo = v.findViewById<ImageView>(R.id.imPhoto)
        val txAlamat = v.findViewById<TextView>(R.id.txAlamat)
        val txTelp = v.findViewById<TextView>(R.id.txAlamat)
        val txJK = v.findViewById<TextView>(R.id.txJK)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout) //new
    }
}